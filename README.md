# locator: A simple IP exposer

This is a very tiny project which returns the IP address of anyone who sends a
request.  
The project has been assigned to @debMan to join the ExaLab team.

## Develop run

### Run server

``` bash
pip install -U pip virtualenv
virtualenv env || python3 -m virtualenv env
source ./env/bin/activate
pip install -r ./requirements.txt
python3 app.py  
# or add trailing & to send the server logs to background
# python3 app.py &
```

### Request

``` bash
curl localhost:5000/apiv1/ip
```

### Expected output

```
{"ip":"127.0.0.1"}
```

## Dockerized

### Pull the image and run

``` bash
docker image pull registry.gitlab.com/debman/locator:latest
docker container run --rm -p 80:8080 registry.gitlab.com/debman/locator:latest
# use -d to run as a daemon
```

### Or build the image and run 
``` bash
docker image build -t locator:dev .
docker container run --rm -p 80:8080 locator:dev 
# use -d to run as a daemon
```

### Request

``` bash
curl localhost/apiv1/ip
```

### Expected output

```
{"ip":"127.0.0.1"}
```

## To-Do

**NOTE:** The application is under development. So, the following items
should be implemented:

- [x] Develop the app using [Flask](https://flask.palletsprojects.com/en/1.1.x)
- [ ] ~~Write tests~~
- [ ] ~~Exceptions and errors handling~~
- [ ] ~~Prepare config file for listen port and some more variables~~
- [x] Dockerize the application
- [x] Enhance the application for production
- [x] Prepare `.gitlab-ci.yml`
- [ ] Prepare the `k8s` deploy stack
- [ ] Find a third-party app to release

